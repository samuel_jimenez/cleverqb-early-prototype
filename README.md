README cleverQb Early Prototype.
===================

The following code consists on two Android Augmented Reality prototypes that use the **Vuforia framework** to track a set of frame markers and register 3D models that react when are collided.

The apps are an early prototype developed for the experiment performed by [Boletsis, Costas, and Simon Mccallum. "Augmented reality cube game for cognitive training: an interaction study."](http://www.ansatt.hig.no/konstantinosb/profile/publications/2014-arcube.pdf).


Different variants of the code were developed afterwards based on the requirements of the experiments performed with it, i.e. different collision tasks and 3D models.

* The **FrameMarkersCubes2dColors** project's objective is to place together a pair of frame markers which are equal, colliding them by equal figure, i.e. a sphere figure should collide with another sphere, and afterwards perform the same task but joining together the figure's colors instead, i.e. a red sphere with red cube.
When two corresponding frame markers collide correctly, a "Correct" message is shown instead of the figures. The time and participants names are saved in a web server.

* The **LettersFrameMarkers3** project is more flexible as the intention is to create words with the letters given, the user is free to choose which letters to use according to the instructions of the observer.
The application only shows the registered 3D letter on top of each frame marker when tracked. The time, score and participants names are recorded as well.



- - -
Remarks:
======================

The prototype basis is the [Frame Markers sample app](https://developer.vuforia.com/resources/sample-apps/frame-markers-sample-app) which was modified for our purpose to show a message 
when a set of corresponding pairs of markers collide each other and different 3D models are registered for each frame marker.

The main rendering engine uses JNI to compile C++ code that performs the augmented reality logic to subsequently link to the java app.

A set of cubes with a different frame marker on each face where printed and built. 

The frame markers used by the application can be found [here](https://bitbucket.org/samuel_jimenez/cleverqb-early-prototype/src/81d829af8f4a96546742bcb218beb072e9b4be0d/A4markers20-29White.pdf?at=master), and additional frame markers supported can be found inside the '/assets' folder of the Vuforia SDK installation folder.


The application is intended to work with the frame markers: **0,1,2,3,24,25,26,27,28**.


Future development will follow this direction: [Project summary]( https://dl.dropboxusercontent.com/u/17733678/smartkuber/smartkuber-general.pdf), [Project Details (design document - all rights reserved)](https://dl.dropboxusercontent.com/u/17733678/smartkuber/smartkuber.pdf)

- - -
Requirements (tested on): 
======================

* Android 4.2.
* [Vuforia 2.8 package for Android](https://developer.vuforia.com/resources/sdk/android).
* [Frames Markers](https://developer.vuforia.com/resources/sdk/android)
* Eclipse IDE with Android development tools.

- - -
Instructions: 
======================

* Fork the project on your computer.
* Print the [set of frame markers](https://bitbucket.org/samuel_jimenez/cleverqb-early-prototype/src/81d829af8f4a96546742bcb218beb072e9b4be0d/A4markers20-29White.pdf?at=master) and build cubes with them (optional).
* Import the project as an **Existing Android Project** into your Eclipse workspace. The folders **FrameMarkersCubes2dColors**, **LettersFrameMarkers3** are separate projects.
* Connect a compatible Android smart-phone (minimum v4.0) with debugging developer options enabled and verify it is available on the Eclipse's DDMS Perspective.
* Run the application on your Android device.
	* To modify the frame markers to use and the basic logic of the interaction, you can go to `jni/FrameMarkers.cpp` and re-compile the C++ code.
	* The 3D models are loaded from `src/com/cleverqb/letters3/FrameMarkers.java`


For more information about the code you can drop me an email to ***sam.jm1618@gmail.com***, or refer to the main researcher ***[Costas Boletsis](http://www.ansatt.hig.no/konstantinosb/)*** for information about the project.

**CleverQb Early Prototype is licensed under the [GPL V3](http://www.gnu.org/copyleft/gpl.html) license. Vuforia is licensed under the [Vuforia License Agreement](https://developer.vuforia.com/legal/vuforia-30-license-agreement).**
