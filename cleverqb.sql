
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cleverqb`
--
CREATE DATABASE IF NOT EXISTS `cleverqb` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `cleverqb`;
-- --------------------------------------------------------

--
-- Table structure for table `scores_figures`
--

CREATE TABLE IF NOT EXISTS `scores_figurescolors` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USR_NAME` varchar(100) NOT NULL,
  `USR_TIME` varchar(100) NOT NULL,
  `USR_SCORE` int(11) DEFAULT NULL,
  `DATE_CREATION` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------


-- --------------------------------------------------------

--
-- Table structure for table `scores_figures`
--

CREATE TABLE IF NOT EXISTS `scores_figures` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USR_NAME` varchar(100) NOT NULL,
  `USR_TIME` varchar(100) NOT NULL,
  `USR_SCORE` int(11) DEFAULT NULL,
  `DATE_CREATION` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `scores_letters1`
--

CREATE TABLE IF NOT EXISTS `scores_letters1` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USR_NAME` varchar(100) NOT NULL,
  `USR_TIME` varchar(100) NOT NULL,
  `USR_SCORE` int(11) DEFAULT NULL,
  `DATE_CREATION` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

-- --------------------------------------------------------

--
-- Table structure for table `scores_letters2`
--

CREATE TABLE IF NOT EXISTS `scores_letters2` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USR_NAME` varchar(100) NOT NULL,
  `USR_TIME` varchar(100) NOT NULL,
  `USR_SCORE` int(11) DEFAULT NULL,
  `DATE_CREATION` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

