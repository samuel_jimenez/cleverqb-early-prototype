/*==============================================================================
Copyright (c) 2010-2013 QUALCOMM Austria Research Center GmbH.
All Rights Reserved.

@file 
    FrameMarkers.cpp

@brief
    Sample for FrameMarkers

==============================================================================*/


#include <jni.h>
#include <android/log.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>

#include <QCAR/QCAR.h>
#include <QCAR/CameraDevice.h>
#include <QCAR/Renderer.h>
#include <QCAR/VideoBackgroundConfig.h>
#include <QCAR/Trackable.h>
#include <QCAR/TrackableResult.h>
#include <QCAR/MarkerResult.h>
#include <QCAR/Tool.h>
#include <QCAR/MarkerTracker.h>
#include <QCAR/TrackerManager.h>
#include <QCAR/CameraCalibration.h>
#include <QCAR/Marker.h>

#include "SampleUtils.h"
#include "Texture.h"
#include "CubeShaders.h"
//#include "monkey_cornelius.h"	//monkey
//#include "glass.h"	//vase
//#include "sss.h"	//cube

//#include "sphere.h"
//#include "torus.h"
//#include "Teapot.h"
//#include "head.h"
//#include "blankModel.h"
//#include "Eobj.h"
//#include "purpletext.h"

//TODO REAL ATTEMP
//#include "cone1.h"
#include "cube1.h"
//#include "cylinder1.h"
//#include "sphere1.h"
#include "correct.h"
#include "triangle1.h"
#include "circle1.h"
//#include "rect1.h"
#include "gengon1.h"
//For distance measure
#include "SampleMath.h"
#include <math.h>

// Textures:
int textureCount                = 0;
Texture** textures              = 0;


#ifdef __cplusplus
extern "C"
{
#endif


// OpenGL ES 2.0 specific:
unsigned int shaderProgramID    = 0;
GLint vertexHandle              = 0;
GLint normalHandle              = 0;
GLint textureCoordHandle        = 0;
GLint mvpMatrixHandle           = 0;
GLint texSampler2DHandle        = 0;

// Screen dimensions:
unsigned int screenWidth        = 0;
unsigned int screenHeight       = 0;

// Indicates whether screen is in portrait (true) or landscape (false) mode
bool isActivityInPortraitMode   = false;

// The projection matrix used for rendering virtual objects:
QCAR::Matrix44F projectionMatrix;

// Constants:
static const float kLetterScale        = 25.0f;
static const float kLetterTranslate    = 25.0f;

//MARKER SKIP, USED TO SKIP THE FIRST MARKER AFTER HAS BEEN DETECTED AND NOT TO SHOW MARKER WHEN IS COLLIDE
int skipRef_1 = 0; //0 - no skip
int skipRef_2 = 0; //0 - no skip
int skipRef_3 = 0; //0 - no skip
int skipRef_4 = 0; //0 - no skip
//int skipRef_5 = 0; //0 - no skip

float distance_1 = 1000.0;
float distance_2 = 1000.0;
float distance_3 = 1000.0;
float distance_4 = 1000.0;
//float distance_5 = 1000.0;


//Object Scale (from ImageTargets)
static/* const*/ float kObjectScale = 0.4f;

static const int MARKER_0 = 0;
static const int MARKER_1 = 1;
static const int MARKER_2 = 2;
static const int MARKER_3 = 3;
static const int MARKER_4 = 24;
static const int MARKER_5 = 25;
static const int MARKER_6 = 26;
static const int MARKER_7 = 27;

static const int THRESH = 75;

JNIEXPORT void JNICALL
Java_com_cleverqb_figurescolors_FrameMarkers_setActivityPortraitMode(JNIEnv *, jobject, jboolean isPortrait)
{
    isActivityInPortraitMode = isPortrait;
}


JNIEXPORT int JNICALL
Java_com_cleverqb_figurescolors_FrameMarkers_initTracker(JNIEnv *, jobject)
{
    LOG("Java_com_cleverqb_figurescolors_FrameMarkers_initTracker");
    
    // Initialize the marker tracker:
    QCAR::TrackerManager& trackerManager = QCAR::TrackerManager::getInstance();
    QCAR::Tracker* trackerBase = trackerManager.initTracker(QCAR::Tracker::MARKER_TRACKER);
    QCAR::MarkerTracker* markerTracker = static_cast<QCAR::MarkerTracker*>(trackerBase);
    if (markerTracker == NULL)
    {
        LOG("Failed to initialize MarkerTracker.");
        return 0;
    }
    
    if (!markerTracker->createFrameMarker(0, "MarkerQ", QCAR::Vec2F(50,50)))
    {
        LOG("Failed to create frame marker Q.");
    }
    if (!markerTracker->createFrameMarker(1, "MarkerC", QCAR::Vec2F(50,50)))
    {
        LOG("Failed to create frame marker C.");
    }
    if (!markerTracker->createFrameMarker(2, "MarkerA", QCAR::Vec2F(50,50)))
    {
        LOG("Failed to create frame marker A.");
    }
    if (!markerTracker->createFrameMarker(3, "MarkerR", QCAR::Vec2F(50,50)))
    {
        LOG("Failed to create frame marker R.");
    }
    //TODO NEW MARKERS
    for(int i=24; i<28;i++)
    {
    	char name = (char)i;
    	const char* mkrId = &name;
        if (!markerTracker->createFrameMarker(i, mkrId, QCAR::Vec2F(50,50)))
        {
            LOG("Failed to create frame marker %i",i);
        }
    }

    LOG("Successfully initialized MarkerTracker.");

    return 1;
}


JNIEXPORT void JNICALL
    Java_com_cleverqb_figurescolors_FrameMarkers_deinitTracker(JNIEnv *, jobject)
{
    LOG("Java_com_cleverqb_figurescolors_FrameMarkers_deinitTracker");

    // Deinit the marker tracker, this will destroy all created frame markers:
    QCAR::TrackerManager& trackerManager = QCAR::TrackerManager::getInstance();
    trackerManager.deinitTracker(QCAR::Tracker::MARKER_TRACKER);
}


JNIEXPORT void JNICALL
Java_com_cleverqb_figurescolors_FrameMarkersRenderer_renderFrame(JNIEnv *, jobject)
{
    //LOG("Java_com_cleverqb_figurescolors_GLRenderer_renderFrame");
 
    // Clear color and depth buffer 
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Get the state from QCAR and mark the beginning of a rendering section
    QCAR::State state = QCAR::Renderer::getInstance().begin();

    // Explicitly render the Video Background
    QCAR::Renderer::getInstance().drawVideoBackground();
    
    glEnable(GL_DEPTH_TEST);

    // We must detect if background reflection is active and adjust the culling direction. 
    // If the reflection is active, this means the post matrix has been reflected as well,
    // therefore standard counter clockwise face culling will result in "inside out" models. 
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    if(QCAR::Renderer::getInstance().getVideoBackgroundConfig().mReflection == QCAR::VIDEO_BACKGROUND_REFLECTION_ON)
        glFrontFace(GL_CW);  //Front camera
    else
        glFrontFace(GL_CCW);   //Back camera



    /*Model matrix and target centers init*/
    QCAR::Matrix44F mainModelViewMatrix;
    QCAR::Vec3F targetCenters_1[2]; //big enough to hold all the targets needed

    //targets for each pair
    QCAR::Vec3F targetCenters_2[2]; //big enough to hold all the targets needed
    QCAR::Vec3F targetCenters_3[2]; //big enough to hold all the targets needed
    QCAR::Vec3F targetCenters_4[2]; //big enough to hold all the targets needed
    QCAR::Vec3F targetCenters_5[2]; //big enough to hold all the targets needed
    int activePair_2 = 0; //0: no pair, 1: is pair
    int activePair_3 = 0; //0: no pair, 1: is pair
    int activePair_4 = 0; //0: no pair, 1: is pair
    int activePair_5 = 0; //0: no pair, 1: is pair

    LOG("---OUTSIDE FOR---- skipFst2: %i",skipRef_2);
	LOG("---OUTSIDE FOR---- distance2: %f",distance_2);



	int exists_1 = 0;
	int exists_2 =0;
	int exists_3 = 0;
	int exists_4 = 0;
//	int exists_5 = 0;
	for(int tIdx = 0; tIdx < state.getNumTrackableResults(); tIdx++){
		const QCAR::TrackableResult* trackableResult = state.getTrackableResult(tIdx);

		// Check the type of the trackable:
		assert(trackableResult->getType() == QCAR::TrackableResult::MARKER_RESULT);
		const QCAR::MarkerResult* markerResult = static_cast<const QCAR::MarkerResult*>(trackableResult);
		const QCAR::Marker& marker = markerResult->getTrackable();

		if(marker.getMarkerId()==MARKER_1){
			exists_1=1;
		}
		if(marker.getMarkerId()==MARKER_3){//TARGETS
			exists_2=1;
		}
		if(marker.getMarkerId()==MARKER_5){
			exists_3=1;
		}
		if(marker.getMarkerId()==MARKER_7){
			exists_4=1;
		}
//		if(marker.getMarkerId()==MARKER_9){
//			exists_5=1;
//		}
	}

	if(exists_1==0){
		distance_1 = 1000.0;
	}
	if(exists_2==0){
		distance_2 = 1000.0;
	}
	if(exists_3==0){
		distance_3 = 1000.0;
	}
	if(exists_4==0){
		distance_4 = 1000.0;
	}
//	if(exists_5==0){
//		distance_5 = 1000.0;
//	}

	// Did we find any trackables this frame?
    LOG("---INIT TRACK");
    for(int tIdx = 0; tIdx < state.getNumTrackableResults(); tIdx++)
    {
        // Get the trackable:
        const QCAR::TrackableResult* trackableResult = state.getTrackableResult(tIdx);
        QCAR::Matrix44F modelViewMatrix =
            QCAR::Tool::convertPose2GLMatrix(trackableResult->getPose());

        // Choose the texture based on the target name:
        int textureIndex = 0;

        // Check the type of the trackable:
        assert(trackableResult->getType() == QCAR::TrackableResult::MARKER_RESULT);
        const QCAR::MarkerResult* markerResult = static_cast<
                                    const QCAR::MarkerResult*>(trackableResult);
        const QCAR::Marker& marker = markerResult->getTrackable();

        //textureIndex = marker.getMarkerId();

        // Select which model to draw:
        const GLvoid* vertices = 0;
        const GLvoid* normals = 0;
        const GLvoid* indices = 0;
        const GLvoid* texCoords = 0;
        int numIndices = 0;

        switch (marker.getMarkerId())
        {
			case MARKER_0://TRIANGLE1
				vertices = &trigwnoVertices[0];//&corneliusVertices[0];
				normals = &trigwnoNormals[0];
				indices = &trigwnoIndices[0];
				texCoords = &trigwnoTexCoords[0];
				numIndices = NUM_TRIGWNO_OBJECT_INDEX;
				textureIndex = 0;//Set texture manually
				break;
			case MARKER_1://TRIANGLE2
				vertices = &gengon001Vertices[0];
				normals = &gengon001Normals[0];
				indices = &gengon001Indices[0];
				texCoords = &gengon001TexCoords[0];
				numIndices = NUM_GENGON001_OBJECT_INDEX;
				textureIndex = 6;
				break;
			case MARKER_2:
				vertices = &trigwnoVertices[0];//&corneliusVertices[0];
				normals = &trigwnoNormals[0];
				indices = &trigwnoIndices[0];
				texCoords = &trigwnoTexCoords[0];
				numIndices = NUM_TRIGWNO_OBJECT_INDEX;
				textureIndex = 1;
				break;
			case MARKER_3:
				vertices = &kuklosVertices[0];
				normals = &kuklosNormals[0];
				indices = &kuklosIndices[0];
				texCoords = &kuklosTexCoords[0];
				numIndices = NUM_KUKLOS_OBJECT_INDEX;
				textureIndex = 5;
				break;
			case MARKER_4://CIRCLE1
				vertices = &cubeVertices[0];
				normals = &cubeNormals[0];
				indices = &cubeIndices[0];
				texCoords = &cubeTexCoords[0];
				numIndices = NUM_CUBE_OBJECT_INDEX;
				textureIndex = 2;
				break;
			case MARKER_5://CIRCLE2
				vertices = &kuklosVertices[0];
				normals = &kuklosNormals[0];
				indices = &kuklosIndices[0];
				texCoords = &kuklosTexCoords[0];
				numIndices = NUM_KUKLOS_OBJECT_INDEX;
				textureIndex = 4;
				break;
			case MARKER_6://RECTANGLE1
				vertices = &cubeVertices[0];
				normals = &cubeNormals[0];
				indices = &cubeIndices[0];
				texCoords = &cubeTexCoords[0];
				numIndices = NUM_CUBE_OBJECT_INDEX;
				textureIndex = 3;
				break;
			case MARKER_7://RECTANGLE2
				vertices = &gengon001Vertices[0];
				normals = &gengon001Normals[0];
				indices = &gengon001Indices[0];
				texCoords = &gengon001TexCoords[0];
				numIndices = NUM_GENGON001_OBJECT_INDEX;
				textureIndex = 7;
				break;
        }


		QCAR::Matrix44F modelViewProjection;


		LOG("---MARKER ID: %i", marker.getMarkerId());
		//Init target centers
		if (marker.getMarkerId() == MARKER_0 ||
			marker.getMarkerId() == MARKER_2 ||
			marker.getMarkerId() == MARKER_4 ||
			marker.getMarkerId() == MARKER_6 /*||
			marker.getMarkerId() == MARKER_8*/)
		{
				// Make the first visible target our world center (0, 0, 0)
				// Store its modelViewMatrix and continue looking for other targets
				mainModelViewMatrix = modelViewMatrix;

				switch(marker.getMarkerId())
				{
					case MARKER_0:
						targetCenters_1[0].data[0] = 0.0f;//x
						targetCenters_1[0].data[1] = 0.0f;//y
						targetCenters_1[0].data[2] = 0.0f;//z
						break;
					case MARKER_2:
						targetCenters_2[0].data[0] = 0.0f;//x
						targetCenters_2[0].data[1] = 0.0f;//y
						targetCenters_2[0].data[2] = 0.0f;//z
						activePair_2 = 1;
						break;
					case MARKER_4:
						targetCenters_3[0].data[0] = 0.0f;//x
						targetCenters_3[0].data[1] = 0.0f;//y
						targetCenters_3[0].data[2] = 0.0f;//z
						activePair_3 = 1;
						break;
					case MARKER_6:
						targetCenters_4[0].data[0] = 0.0f;//x
						targetCenters_4[0].data[1] = 0.0f;//y
						targetCenters_4[0].data[2] = 0.0f;//z
						activePair_4 = 1;
						break;
//					case MARKER_8:
//						targetCenters_5[0].data[0] = 0.0f;//x
//						targetCenters_5[0].data[1] = 0.0f;//y
//						targetCenters_5[0].data[2] = 0.0f;//z
//						activePair_5 = 1;
						break;

				}
		}
		else //	//// -----------------------  if (marker.getMarkerId() == secondMarker)
		{
				// This is another visible target
				// Find its center point in relation to the first target
				// To do this we use the matrix inverse function (SampleMath.h from the Dominoes project)
				QCAR::Matrix44F mainModelViewInverse = SampleMath::Matrix44FInverse(mainModelViewMatrix);
				QCAR::Matrix44F modelViewTranspose = SampleMath::Matrix44FTranspose(modelViewMatrix); // let's work with row-major matrices
				QCAR::Matrix44F offsetMatrix = QCAR::Tool::multiply(mainModelViewInverse, modelViewTranspose);

				// Transform a point on the second target by this offset matrix
				// (0, 0, 0) is the local center of the target
				QCAR::Vec4F position(0.0f, 0.0f, 0.0f, 1.0f);
				position = SampleMath::Vec4FTransform(position, offsetMatrix);

				if(marker.getMarkerId() == MARKER_1){
					// Add this position to our array
					targetCenters_1[1].data[0] = position.data[0];
					targetCenters_1[1].data[1] = position.data[1];
					targetCenters_1[1].data[2] = position.data[2];

					LOG("---TARGET MARKER  x: %f, y:%f, z:%f ", targetCenters_1[1].data[0], targetCenters_1[1].data[1], targetCenters_1[1].data[2]);
					LOG("---REFERENCE MARKER x: %f, y:%f, z:%f ", targetCenters_1[0].data[0], targetCenters_1[0].data[1], targetCenters_1[0].data[2]);

					distance_1 = sqrt(targetCenters_1[1].data[0]*targetCenters_1[1].data[0] + targetCenters_1[1].data[1]*targetCenters_1[1].data[1] + targetCenters_1[1].data[2]*targetCenters_1[1].data[2]);
					LOG("---DISTANCE1 XYZ: %f",distance_1);

				}
				if (marker.getMarkerId() == MARKER_3 && activePair_2==1){ //WHEN MARKER 0 & 21 COLLIDE, IT WONT SHOW 21
					// Add this position to our array
					targetCenters_2[1].data[0] = position.data[0];
					targetCenters_2[1].data[1] = position.data[1];
					targetCenters_2[1].data[2] = position.data[2];

					distance_2 = sqrt(targetCenters_2[1].data[0]*targetCenters_2[1].data[0] + targetCenters_2[1].data[1]*targetCenters_2[1].data[1] + targetCenters_2[1].data[2]*targetCenters_2[1].data[2]);
					LOG("---DISTANCE2 XYZ: %f",distance_2);
				}

				if (marker.getMarkerId() == MARKER_5 && activePair_3==1){
					// Add this position to our array
					targetCenters_3[1].data[0] = position.data[0];
					targetCenters_3[1].data[1] = position.data[1];
					targetCenters_3[1].data[2] = position.data[2];

					distance_3 = sqrt(targetCenters_3[1].data[0]*targetCenters_3[1].data[0] + targetCenters_3[1].data[1]*targetCenters_3[1].data[1] + targetCenters_3[1].data[2]*targetCenters_3[1].data[2]);
					LOG("---DISTANCE3 XYZ: %f",distance_3);
				}
				if (marker.getMarkerId() == MARKER_7 && activePair_4==1){
					// Add this position to our array
					targetCenters_4[1].data[0] = position.data[0];
					targetCenters_4[1].data[1] = position.data[1];
					targetCenters_4[1].data[2] = position.data[2];

					distance_4 = sqrt(targetCenters_4[1].data[0]*targetCenters_4[1].data[0] + targetCenters_4[1].data[1]*targetCenters_4[1].data[1] + targetCenters_4[1].data[2]*targetCenters_4[1].data[2]);
					LOG("---DISTANCE2 XYZ: %f",distance_4);
				}
//				if (marker.getMarkerId() == MARKER_9 && activePair_5==1){
//					// Add this position to our array
//					targetCenters_5[1].data[0] = position.data[0];
//					targetCenters_5[1].data[1] = position.data[1];
//					targetCenters_5[1].data[2] = position.data[2];
//
//					distance_5 = sqrt(targetCenters_5[1].data[0]*targetCenters_5[1].data[0] + targetCenters_5[1].data[1]*targetCenters_5[1].data[1] + targetCenters_5[1].data[2]*targetCenters_5[1].data[2]);
//					LOG("---DISTANCE2 XYZ: %f",distance_5);
//				}
		}

//		if(distance2!=distanceTemp){ //means that it does not clculate in this run
//			distance2 = distanceTemp;
//		}
		//quiere decir que acban de pasar, entonces estamos en el 2

		//distance2 = isnanf(distance2)==0?100:distance_2;

//		skipFirst2 = distance2<THRESH? 1:0;

		//CHANGED FROM SAMPLEGUIDEWEB THE PROBLEM WAS THE COORDINATES
		SampleUtils::translatePoseMatrix(0.0f, 0.0f, 0.0f,&modelViewMatrix.data[0]);
		SampleUtils::rotatePoseMatrix(-30.0,1.0,0.0,0.0,&modelViewMatrix.data[0]);
		SampleUtils::scalePoseMatrix(kObjectScale, kObjectScale, kObjectScale,&modelViewMatrix.data[0]);
		SampleUtils::multiplyMatrix(&projectionMatrix.data[0],&modelViewMatrix.data[0],&modelViewProjection.data[0]);


		LOG("---DISTANCES 1 = %f ::::: 2:=  %f",distance_1, distance_2);

		if(distance_1 < THRESH && marker.getMarkerId()==MARKER_1){
				LOG("---MARKER1 ID (inside statement): %i", marker.getMarkerId());
				LOG("---CLOSE OBJECTS ON x DIRECTION x:%f", targetCenters_1[1].data[0]);
				skipRef_1 = 1;
				/////FOR 2ND ONLY
				SampleUtils::translatePoseMatrix(targetCenters_1[1].data[0] *-0.8f,
												 targetCenters_1[1].data[1] *-0.8f,
												 targetCenters_1[1].data[2],
												&modelViewMatrix.data[0]);	//translation on x,y,z and matrix
				SampleUtils::rotatePoseMatrix(60.0,1.0,0.0,0.0,&modelViewMatrix.data[0]);
				SampleUtils::scalePoseMatrix(2.0f, 2.0f, 2.0f, &modelViewMatrix.data[0]);
				SampleUtils::multiplyMatrix(&projectionMatrix.data[0],&modelViewMatrix.data[0] ,
																&modelViewProjection.data[0]);

				SampleUtils::printMatrix(&modelViewProjection.data[0]);
				vertices = &textVertices[0];
				normals = &textNormals[0];
				numIndices = NUM_TEXT_OBJECT_INDEX;
				indices = &textIndices[0];
				texCoords = &textTexCoords[0];
				textureIndex = 8;

    	}else if(distance_2 < THRESH && marker.getMarkerId()==MARKER_3){
				LOG("---MARKER2 ID (inside statement): %i", marker.getMarkerId());
				LOG("---CLOSE OBJECTS ON x DIRECTION x:%f", targetCenters_2[1].data[0]);
				skipRef_2 = 1;
				SampleUtils::translatePoseMatrix(targetCenters_2[1].data[0] *-0.8f,
												 targetCenters_2[1].data[1] *-0.8f,
												 targetCenters_2[1].data[2],
												&modelViewMatrix.data[0]);	//translation on x,y,z and matrix
				SampleUtils::rotatePoseMatrix(60.0,1.0,0.0,0.0,&modelViewMatrix.data[0]);
				SampleUtils::scalePoseMatrix(2.0f, 2.0f, 2.0f, &modelViewMatrix.data[0]);
				SampleUtils::multiplyMatrix(&projectionMatrix.data[0],&modelViewMatrix.data[0] ,
																&modelViewProjection.data[0]);

				vertices = &textVertices[0];
				normals = &textNormals[0];
				numIndices = NUM_TEXT_OBJECT_INDEX;
				indices = &textIndices[0];
				texCoords = &textTexCoords[0];
				textureIndex = 8;
    	}else if(distance_3 < THRESH && marker.getMarkerId()==MARKER_5){
				skipRef_3 = 1;
				SampleUtils::translatePoseMatrix(targetCenters_3[1].data[0] *-0.8f,
												 targetCenters_3[1].data[1] *-0.8f,
												 targetCenters_3[1].data[2],
												&modelViewMatrix.data[0]);	//translation on x,y,z and matrix
				SampleUtils::rotatePoseMatrix(60.0,1.0,0.0,0.0,&modelViewMatrix.data[0]);
				SampleUtils::scalePoseMatrix(2.0f, 2.0f, 2.0f, &modelViewMatrix.data[0]);
				SampleUtils::multiplyMatrix(&projectionMatrix.data[0],&modelViewMatrix.data[0] ,
																&modelViewProjection.data[0]);

				vertices = &textVertices[0];
				normals = &textNormals[0];
				numIndices = NUM_TEXT_OBJECT_INDEX;
				indices = &textIndices[0];
				texCoords = &textTexCoords[0];
				textureIndex = 8;
    	}else if(distance_4 < THRESH && marker.getMarkerId()==MARKER_7){
				skipRef_4 = 1;
				SampleUtils::translatePoseMatrix(targetCenters_4[1].data[0] *-0.8f,
												 targetCenters_4[1].data[1] *-0.8f,
												 targetCenters_4[1].data[2],// <0? 100.0f: -100.0f,
												&modelViewMatrix.data[0]);	//translation on x,y,z and matrix
				SampleUtils::rotatePoseMatrix(60.0,1.0,0.0,0.0,&modelViewMatrix.data[0]);
				SampleUtils::scalePoseMatrix(2.0f, 2.0f, 2.0f, &modelViewMatrix.data[0]);
				SampleUtils::multiplyMatrix(&projectionMatrix.data[0],&modelViewMatrix.data[0] ,
																&modelViewProjection.data[0]);


				vertices = &textVertices[0];
				normals = &textNormals[0];
				numIndices = NUM_TEXT_OBJECT_INDEX;
				indices = &textIndices[0];
				texCoords = &textTexCoords[0];
				textureIndex = 8;
		}
//    	else if(distance_5 < THRESH && marker.getMarkerId()==MARKER_9){
//				skipRef_5 = 1;
//				SampleUtils::translatePoseMatrix(targetCenters_5[1].data[0] <0? 3.0f: -3.0f,
//												 targetCenters_5[1].data[1] <0? 1.0f: -1.0f,
//												 targetCenters_5[1].data[2] <0? 3.0f: -3.0f,
//												&modelViewMatrix.data[0]);	//translation on x,y,z and matrix
//				SampleUtils::scalePoseMatrix(3.0f, 3.0f, 3.0f, &modelViewMatrix.data[0]);
//				SampleUtils::multiplyMatrix(&projectionMatrix.data[0],&modelViewMatrix.data[0] ,
//																&modelViewProjection.data[0]);
//
//				vertices = &cubeVertices[0];
//				normals = &cubeNormals[0];
//				numIndices = NUM_CUBE_OBJECT_INDEX;
//				indices = &cubeIndices[0];
//		}

//		if(skipRef_1 ==1 || skipRef_2 ==1 || skipRef_3 ==1 || skipRef_4 ==1){
//			textureIndex = 8;
//		}

		assert(textureIndex < textureCount);
		const Texture* const thisTexture = textures[textureIndex];
		LOG("---Texture: %i:",textureIndex);

		if (marker.getMarkerId()==MARKER_2 && skipRef_2 ==1){
    		LOG("BLANK MARKER2");
    	}else if (marker.getMarkerId()==MARKER_0 && skipRef_1 ==1){
    		LOG("BLANK MARKER1");
    	}else if (marker.getMarkerId()==MARKER_4 && skipRef_3 ==1){
    		LOG("BLANK MARKER3");
    	}else if (marker.getMarkerId()==MARKER_6 && skipRef_4 ==1){
    		LOG("BLANK MARKER4");
    	}/*else if (marker.getMarkerId()==MARKER_8 && skipRef_5 ==1){
    		LOG("BLANK MARKER5");
    	}*/else{
			LOG("---NORMAL FLOW");

			glUseProgram(shaderProgramID);
			glVertexAttribPointer(vertexHandle, 3, GL_FLOAT, GL_FALSE, 0, vertices);
			glVertexAttribPointer(normalHandle, 3, GL_FLOAT, GL_FALSE, 0, normals);
			glVertexAttribPointer(textureCoordHandle, 2, GL_FLOAT, GL_FALSE, 0, texCoords);

			glEnableVertexAttribArray(vertexHandle);
			glEnableVertexAttribArray(normalHandle);
			glEnableVertexAttribArray(textureCoordHandle);

			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, thisTexture->mTextureID); //id of the texture (loadTExture on FrameMarkers.java)
			glUniformMatrix4fv(mvpMatrixHandle, 1, GL_FALSE,(GLfloat*)&modelViewProjection.data[0]);
			glUniform1i(texSampler2DHandle, 0 /*GL_TEXTURE0*/);
			glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_SHORT, indices);

			glDisableVertexAttribArray(vertexHandle);
			glDisableVertexAttribArray(normalHandle);
			glDisableVertexAttribArray(textureCoordHandle);

			glUseProgram(shaderProgramID);
    	}

		if(distance_1>THRESH){
			skipRef_1 = 0;
		}

		if(distance_2>THRESH){
			skipRef_2 = 0;
		}
		if(distance_3>THRESH){
			skipRef_3 = 0;
		}
		if(distance_4>THRESH){
			skipRef_4 = 0;
		}
//		if(distance_5>THRESH){
//			skipRef_5 = 0;
//		}

		LOG("---SKIP 1: %i --- SKIP 2: %i",skipRef_1, skipRef_2);

        SampleUtils::checkGlError("FrameMarkers render frame");
    }

    glDisable(GL_DEPTH_TEST);

    QCAR::Renderer::getInstance().end();
}


void
configureVideoBackground()
{
    // Get the default video mode:
    QCAR::CameraDevice& cameraDevice = QCAR::CameraDevice::getInstance();
    QCAR::VideoMode videoMode = cameraDevice.
                                getVideoMode(QCAR::CameraDevice::MODE_DEFAULT);

    // Configure the video background
    QCAR::VideoBackgroundConfig config;
    config.mEnabled = true;
    config.mSynchronous = true;
    config.mPosition.data[0] = 0.0f;
    config.mPosition.data[1] = 0.0f;
    
    if (isActivityInPortraitMode)
    {
        //LOG("configureVideoBackground PORTRAIT");
        config.mSize.data[0] = videoMode.mHeight
                                * (screenHeight / (float)videoMode.mWidth);
        config.mSize.data[1] = screenHeight;

        if(config.mSize.data[0] < screenWidth)
        {
            LOG("Correcting rendering background size to handle missmatch between screen and video aspect ratios.");
            config.mSize.data[0] = screenWidth;
            config.mSize.data[1] = screenWidth * 
                              (videoMode.mWidth / (float)videoMode.mHeight);
        }
    }
    else
    {
        //LOG("configureVideoBackground LANDSCAPE");
        config.mSize.data[0] = screenWidth;
        config.mSize.data[1] = videoMode.mHeight
                            * (screenWidth / (float)videoMode.mWidth);

        if(config.mSize.data[1] < screenHeight)
        {
            LOG("Correcting rendering background size to handle missmatch between screen and video aspect ratios.");
            config.mSize.data[0] = screenHeight
                                * (videoMode.mWidth / (float)videoMode.mHeight);
            config.mSize.data[1] = screenHeight;
        }
    }

    LOG("Configure Video Background : Video (%d,%d), Screen (%d,%d), mSize (%d,%d)", videoMode.mWidth, videoMode.mHeight, screenWidth, screenHeight, config.mSize.data[0], config.mSize.data[1]);

    // Set the config:
    QCAR::Renderer::getInstance().setVideoBackgroundConfig(config);
}


JNIEXPORT void JNICALL
Java_com_cleverqb_figurescolors_FrameMarkers_initApplicationNative(
                            JNIEnv* env, jobject obj, jint width, jint height)
{
    LOG("Java_com_cleverqb_figurescolors_FrameMarkers_initApplicationNative");
    
    // Store screen dimensions
    screenWidth = width;
    screenHeight = height;
        
    // Handle to the activity class:
    jclass activityClass = env->GetObjectClass(obj);

    jmethodID getTextureCountMethodID = env->GetMethodID(activityClass,
                                                    "getTextureCount", "()I");
    if (getTextureCountMethodID == 0)
    {
        LOG("Function getTextureCount() not found.");
        return;
    }

    textureCount = env->CallIntMethod(obj, getTextureCountMethodID);
    LOG("---TextureCount: %i",textureCount);
    if (!textureCount)
    {
        LOG("getTextureCount() returned zero.");
        return;
    }

    textures = new Texture*[textureCount];

    jmethodID getTextureMethodID = env->GetMethodID(activityClass,
        "getTexture", "(I)Lcom/cleverqb/figurescolors/Texture;");

    if (getTextureMethodID == 0)
    {
        LOG("Function getTexture() not found.");
        return;
    }

    // Register the textures
    for (int i = 0; i < textureCount; ++i)
    {

        jobject textureObject = env->CallObjectMethod(obj, getTextureMethodID, i);
        if (textureObject == NULL)
        {
            LOG("GetTexture() returned zero pointer");
            return;
        }

        textures[i] = Texture::create(env, textureObject);
    }
}


JNIEXPORT void JNICALL
Java_com_cleverqb_figurescolors_FrameMarkers_deinitApplicationNative(
                                                        JNIEnv* env, jobject obj)
{
    LOG("Java_com_cleverqb_figurescolors_FrameMarkers_deinitApplicationNative");

    // Release texture resources
    if (textures != 0)
    {
        for (int i = 0; i < textureCount; ++i)
        {
            delete textures[i];
            textures[i] = NULL;
        }

        delete[]textures;
        textures = NULL;

        textureCount = 0;
    }
}


JNIEXPORT void JNICALL
Java_com_cleverqb_figurescolors_FrameMarkers_startCamera(JNIEnv *,
                                                                         jobject)
{
    LOG("Java_com_cleverqb_figurescolors_FrameMarkers_startCamera");
    
    // Select the camera to open, set this to QCAR::CameraDevice::CAMERA_FRONT 
    // to activate the front camera instead.
    QCAR::CameraDevice::CAMERA camera = QCAR::CameraDevice::CAMERA_DEFAULT;

    // Initialize the camera:
    if (!QCAR::CameraDevice::getInstance().init(camera))
        return;

    // Configure the video background
    configureVideoBackground();

    // Select the default mode:
    if (!QCAR::CameraDevice::getInstance().selectVideoMode(
                                QCAR::CameraDevice::MODE_DEFAULT))
        return;

    // Start the camera:
    if (!QCAR::CameraDevice::getInstance().start())
        return;

    // Start the tracker:
    QCAR::TrackerManager& trackerManager = QCAR::TrackerManager::getInstance();
    QCAR::Tracker* markerTracker = trackerManager.getTracker(QCAR::Tracker::MARKER_TRACKER);
    if(markerTracker != 0)
        markerTracker->start();
}


JNIEXPORT void JNICALL
Java_com_cleverqb_figurescolors_FrameMarkers_stopCamera(JNIEnv *,
                                                                   jobject)
{
    LOG("Java_com_cleverqb_figurescolors_FrameMarkers_stopCamera");
    
    // Stop the tracker:
    QCAR::TrackerManager& trackerManager = QCAR::TrackerManager::getInstance();
    QCAR::Tracker* markerTracker = trackerManager.getTracker(QCAR::Tracker::MARKER_TRACKER);
    if(markerTracker != 0)
        markerTracker->stop();
    
    QCAR::CameraDevice::getInstance().stop();
    QCAR::CameraDevice::getInstance().deinit();
}

JNIEXPORT void JNICALL
Java_com_cleverqb_figurescolors_FrameMarkers_setProjectionMatrix(JNIEnv *, jobject)
{
    LOG("Java_com_cleverqb_figurescolors_FrameMarkers_setProjectionMatrix");

    // Cache the projection matrix:
    const QCAR::CameraCalibration& cameraCalibration =
                                QCAR::CameraDevice::getInstance().getCameraCalibration();
    projectionMatrix = QCAR::Tool::getProjectionGL(cameraCalibration, 2.0f, 2500.0f);
}

JNIEXPORT jboolean JNICALL
Java_com_cleverqb_figurescolors_FrameMarkers_autofocus(JNIEnv*, jobject)
{
    return QCAR::CameraDevice::getInstance().setFocusMode(QCAR::CameraDevice::FOCUS_MODE_TRIGGERAUTO) ? JNI_TRUE : JNI_FALSE;
}

JNIEXPORT jboolean JNICALL
Java_com_cleverqb_figurescolors_FrameMarkers_setFocusMode(JNIEnv*, jobject, jint mode)
{
    int qcarFocusMode;

    switch ((int)mode)
    {
        case 0:
            qcarFocusMode = QCAR::CameraDevice::FOCUS_MODE_NORMAL;
            break;
        
        case 1:
            qcarFocusMode = QCAR::CameraDevice::FOCUS_MODE_CONTINUOUSAUTO;
            break;
            
        case 2:
            qcarFocusMode = QCAR::CameraDevice::FOCUS_MODE_INFINITY;
            break;
            
        case 3:
            qcarFocusMode = QCAR::CameraDevice::FOCUS_MODE_MACRO;
            break;
    
        default:
            return JNI_FALSE;
    }
    
    return QCAR::CameraDevice::getInstance().setFocusMode(qcarFocusMode) ? JNI_TRUE : JNI_FALSE;
}


JNIEXPORT void JNICALL
Java_com_cleverqb_figurescolors_FrameMarkersRenderer_initRendering(
                                                    JNIEnv* env, jobject obj)
{
    LOG("Java_com_cleverqb_figurescolors_FrameMarkersRenderer_initRendering");

    // Define clear color
    glClearColor(0.0f, 0.0f, 0.0f, QCAR::requiresAlpha() ? 0.0f : 1.0f);
    
    // Now generate the OpenGL texture objects and add settings
    for (int i = 0; i < textureCount; ++i)
    {
        glGenTextures(1, &(textures[i]->mTextureID));
        glBindTexture(GL_TEXTURE_2D, textures[i]->mTextureID);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, textures[i]->mWidth,
                textures[i]->mHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE,
                (GLvoid*)  textures[i]->mData);
    }

    shaderProgramID     = SampleUtils::createProgramFromBuffer(cubeMeshVertexShader,
                                                            cubeFragmentShader);

    vertexHandle        = glGetAttribLocation(shaderProgramID,
                                                "vertexPosition");
    normalHandle        = glGetAttribLocation(shaderProgramID,
                                                "vertexNormal");
    textureCoordHandle  = glGetAttribLocation(shaderProgramID,
                                                "vertexTexCoord");
    mvpMatrixHandle     = glGetUniformLocation(shaderProgramID,
                                                "modelViewProjectionMatrix");
    texSampler2DHandle  = glGetUniformLocation(shaderProgramID, 
                                                "texSampler2D");
}


JNIEXPORT void JNICALL
Java_com_cleverqb_figurescolors_FrameMarkersRenderer_updateRendering(
                        JNIEnv* env, jobject obj, jint width, jint height)
{
    LOG("Java_com_cleverqb_figurescolors_FrameMarkersRenderer_updateRendering");
    
    // Update screen dimensions
    screenWidth = width;
    screenHeight = height;

    // Reconfigure the video background
    configureVideoBackground();
}


#ifdef __cplusplus
}
#endif


//Basically the Tracker is analyzing the image for patterns of luminance contrast (i.e. light vs dark areas ).
//A FrameMarker presents a very specific and predictable pattern that is easily recognized within the cameras
//field of view. It always has a continuous dark square border, and the ID coding always uses proportionally
//sized light and dark blocks that follow that border. This geometry and arrangement of blocks is distinct,
//which enables the development of a computer vision algorithm to specifically detect frame markers.
